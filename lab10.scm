(define add(lambda (m n s z)(m s (n s z)))) ;addition function
(define sub(lambda(m n)(m pred n))) ;subtraction functiona
(define AND (lambda(m n a b)(n (m a b) b))) ;AND function
(define ORfunc (lambda (m n a b)(n a (m a b)))) ;OR function
(define NOT (lambda (m a b)(m b a))) ;NOT function
(define LEQ (lambda (m n)(IsZero (sub m n)))) ;Less than and equal to function
(define GEQ (lambda (m n)(LEQ n m))) ;Greater than and equal to function
 
